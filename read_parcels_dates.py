import pandas as pd

# data = pd.read_csv("parcels_dates.csv", ',') 
# listData = pd.DataFrame(data) 
# listData.parcela_id.unique()
# listData[lambda x: (x.filtro == 'NDVI') & (x.parcela_id == 1299)] 
# singledata = listData[lambda x: (x.filtro == 'NDVI') & (x.parcela_id == 1299)] 
# singledata.sort_values('data_imagem') 
# singledata.sort_values('data_imagem', ascending = True)
data = pd.read_csv("parcels_dates.csv", ',')
data[['date', 'time']] = data.data_imagem.str.split(' ', expand=True)
parcelID = data.parcela_id.unique()
#filtroName = data.filtro.unique()

newData = pd.DataFrame()

for i in range (0, len(parcelID)):
	#print (parcelList[i])
	filtroName = (data[lambda x: (x.parcela_id == parcelID[i])]).filtro.unique()

	for j in range (0, len(filtroName)):
		parcelData = pd.DataFrame()
		#print (filtroName[j])
		parcelData = (data[lambda x: (x.parcela_id == parcelID[i]) & (x.filtro == filtroName[j])]).sort_values('date')
		parcelData['date'] = pd.to_datetime(parcelData['date'])
		parcelData['date_diff'] = [parcelData['date'].iloc[k] - parcelData['date'].iloc[k - 1] for k in range(0, len(parcelData['date']))]
		parcelData['date_diff'].iloc[0] = '0 days 00:00:00'
		newData = newData.append([parcelData])

#print newData
newData.to_csv('Generate_Diff_Date.csv')



# parcelData = (data[lambda x: (x.filtro == 'NDVI') & (x.parcela_id == 1299)]).sort_values('date')
# parcelData['date'] = pd.to_datetime(parcelData['date'])
# parcelData['date_diff'] = [parcelData['date'].iloc[i] - parcelData['date'].iloc[i - 1] for i in range(0, len(parcelData['date']))]
# parcelData['date_diff'].iloc[0] = '0 days 00:00:00'
